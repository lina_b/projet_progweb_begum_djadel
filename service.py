from flask import Flask, request, jsonify
import sqlite3
from flask_bcrypt import Bcrypt

app = Flask(__name__)
bcrypt = Bcrypt(app)

# Fonction pour établir une connexion SQLite pour chaque demande
def get_db():
    db = sqlite3.connect('passwords.db')
    db.row_factory = sqlite3.Row
    return db

# Création de la table 'users' si elle n'existe pas
def create_users_table():
    with get_db() as db:
        cursor = db.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS users (
                            id INTEGER PRIMARY KEY,
                            username TEXT NOT NULL,
                            password TEXT NOT NULL
                        )''')
        db.commit()

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return '''
        <h1>Welcome to the password manager!</h1>
        <h2>Register</h2>
        <form action="/" method="post">
            Username: <input type="text" name="username"><br>
            Password: <input type="password" name="password"><br>
            <input type="submit" name="action" value="register">
        </form>
        <h2>Login</h2>
        <form action="/" method="post">
            Username: <input type="text" name="username"><br>
            Password: <input type="password" name="password"><br>
            <input type="submit" name="action" value="login">
        </form>
        '''
    elif request.method == 'POST':
        action = request.form.get('action')
        if action == 'register':
            return register(request.form.get('username'), request.form.get('password'))
        elif action == 'login':
            return login(request.form.get('username'), request.form.get('password'))

def register(username, password):
    if not username or not password:
        return jsonify({'error': 'Username and password are required'}), 400

    # Insérer l'utilisateur dans la base de données
    hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')
    with get_db() as db:
        cursor = db.cursor()
        cursor.execute('''INSERT INTO users (username, password) VALUES (?, ?)''', (username, hashed_password))
        db.commit()

    return jsonify({'message': 'User registered successfully'}), 201

def login(username, password):
    if not username or not password:
        return jsonify({'error': 'Username and password are required'}), 400

    # Récupérer l'utilisateur de la base de données
    with get_db() as db:
        cursor = db.cursor()
        cursor.execute('''SELECT * FROM users WHERE username = ?''', (username,))
        user = cursor.fetchone()

    # Vérifier si l'utilisateur existe et si le mot de passe est correct
    if not user or not bcrypt.check_password_hash(user['password'], password):
        return jsonify({'error': 'Invalid username or password'}), 401

    return jsonify({'message': 'Login successful'}), 200

if __name__ == '__main__':
    create_users_table()
    app.run(debug=True, port=5000)

