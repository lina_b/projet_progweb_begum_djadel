# service2.py
from flask import Flask, request, jsonify
import random
import string

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return '''
    <h1>Password Generator</h1>
    <form action="/generate" method="post">
        <input type="submit" value="Generate Password">
    </form>
    '''

@app.route('/generate', methods=['POST'])
def generate_password():
    password_length = 12  # Longueur du mot de passe généré
    password_characters = string.ascii_letters + string.digits + string.punctuation
    generated_password = ''.join(random.choice(password_characters) for i in range(password_length))
    return jsonify({'password': generated_password})

if __name__ == '__main__':
    app.run(debug=True, port=5000)
