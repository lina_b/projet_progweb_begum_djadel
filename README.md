# Projet Programmation Web

## Présentation

Ce projet vise à développer une application web sécurisée de gestion de mots de passe, utilisant des technologies modernes telles que Docker et Kubernetes, et incorporant un mesh de services pour la gestion avancée du trafic. L'application offrira un stockage et une récupération sécurisés des mots de passe.

## Structure du Projet

### Phase de Développement

1. **Développement du Service Web de Base :** Développement d'un service web offrant des fonctionnalités de stockage et de récupération sécurisés des mots de passe.

2. **Ajout d'un deuxième Service :** Mise en place d'une extension permettant de générer des mots de passe aléatoires.

### Conteneurisation

1. **Création du Dockerfile :** Création d'un Dockerfile pour conteneuriser notre application.

2. **Construction et Test du Conteneur Localement :** Construction et test de conteneur localement afin de nous assurer qu'il fonctionne correctement.

3. **Publication de l'Image Docker :** Publication de l'image du conteneur sur Docker Hub pour une distribution facile et une utilisation dans Kubernetes.

### Déploiement Kubernetes

1. **Déploiement de l'Application Conteneurisée :** Déploiement de notre application conteneurisée sur Kubernetes pour une gestion efficace des ressources et une scalabilité facile.

2. **Utilisation de Fichiers YAML :** Utilisation des fichiers YAML pour la gestion du déploiement et des services dans Kubernetes, ce qui facilite la configuration et la gestion.

### Configuration Avancée

1. **Implémentation d'Istio :** Implémentation d'Istio pour la gestion avancée du trafic et la sécurisation des communications au sein de notre application.

2. **Configuration d'une Passerelle :** Configuration d'une passerelle Istio permettant de gérer le trafic réseau entrant et sortant de notre application.

## Technologies & Outils

- Langage de programmation : Python
- Base de données : SQLite3
- Conteneurisation : Docker
- Orchestration : Kubernetes (Minikube pour les tests locaux)
- Mesh de Services : Istio
- Versioning : Git (hébergé sur GitLab)

## Installation et Configuration

### Prérequis

- Docker installé et fonctionnel
- Cluster Kubernetes configuré (utilisation de Minikube pour les tests locaux)
- Istio installé et configuré
- Base de données SQLite3

